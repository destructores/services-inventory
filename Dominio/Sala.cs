﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServicesInventory.Dominio
{
    [DataContract]
    public class Sala
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Referencia { get; set; }

        [DataMember]
        public int Capacidad { get; set; }

        [DataMember]
        public string Contacto { get; set; }

        [DataMember]
        public string Correo { get; set; }

        [DataMember]
        public int Telefono { get; set; }

        [DataMember]
        public string Distrito { get; set; }

        [DataMember]
        public string Zona { get; set; }
    }
}