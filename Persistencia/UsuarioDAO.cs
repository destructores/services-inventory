﻿using ServicesInventory.Dominio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServicesInventory.Persistencia
{
    public class UsuarioDAO
    {
       // private string cadenaConexion = "Data Source=(local)\\SQLEXPRESS;Initial Catalog=ProyectoSALAS01;Integrated Security=SSPI;";

        private string cadenaConexion = "Data Source=(local)\\SQLEXPRESS;Initial Catalog=DBUsuarios;Integrated Security=SSPI;";

        public Usuario ObtenerV(int dni)
        {

            Usuario vendedorEncontrado = null;
           // string sql = "SELECT * from t_vendedor WHERE nu_dni=@dni";
            string sql = "SELECT * from t_usuario WHERE nu_dni=@dni";
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@dni", dni));
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            vendedorEncontrado = new Usuario()
                            {
                                Dni = (int)resultado["nu_dni"],
                                Nombre = (string)resultado["tx_nombre"],
                                Apellido = (string)resultado["tx_apellido"],
                                TelefonoMovil = (int)resultado["nu_telefono"],
                                Zona = (string)resultado["tx_zona"],
                            };
                        }
                    }
                }
            }
            return vendedorEncontrado;

        }


        public List<Usuario> Listar()
        {

            List<Usuario> vendedoresEncontrados = new List<Usuario>();
            Usuario vendedorEncontrado = null;
            //string sql = "SELECT * from t_vendedor";
            string sql = "SELECT * from t_usuario";

            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            vendedorEncontrado = new Usuario()
                            {
                                Dni = (int)resultado["nu_dni"],
                                Nombre = (string)resultado["tx_nombre"],
                                Apellido = (string)resultado["tx_apellido"],
                                TelefonoMovil = (int)resultado["nu_telefono"],
                                Zona = (string)resultado["tx_zona"],
                            };
                            vendedoresEncontrados.Add(vendedorEncontrado);
                        }
                    }
                }
            }
            return vendedoresEncontrados;
        }
    }
}