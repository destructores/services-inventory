﻿using ServicesInventory.Dominio;
using ServicesInventory.Errores;
using ServicesInventory.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServicesInventory
{
    

    public class UsuariosService : IUsuariosService
    {
        private UsuarioDAO usuariodao = new UsuarioDAO();

        public Usuario ObtenerUsuario(int dni)
        {
            Usuario usuario = null;
            

            try
            {
                usuario = usuariodao.ObtenerV(dni);

               
            }
            catch
            {
                throw new FaultException<UsuarioException>(
                   new UsuarioException()
                   {
                       Codigo = "0002",
                       Descripcion = "Error interno"
                   },
                   new FaultReason("Error interno"));
                throw new NotImplementedException();
            }

            if (usuario != null)
            {
                return usuario;
            }
            else
            {
                throw new FaultException<UsuarioException>(
                    new UsuarioException()
                    {
                        Codigo = "0001",
                        Descripcion = "No existe usuario"
                    },
                    new FaultReason("No existe usuario"));
                throw new NotImplementedException();
            }


        }

        public List<Usuario> ListarUsuarios()
        {
            List<Usuario> listarusuario = null;
            listarusuario = usuariodao.Listar();
            try
            {
                return listarusuario;
            }
            catch
            {
                throw new FaultException<UsuarioException>(
                   new UsuarioException()
                   {
                       Codigo = "0002",
                       Descripcion = "Error interno"
                   },
                   new FaultReason("Error interno"));
                throw new NotImplementedException();
            }


        }
    }
}
